#include <cmath>
#include <iostream>
#include <vector>

#include <gnuplot-iostream.h>

template<typename X, typename U>
struct xu_pair {
    xu_pair(X xi_, U ui_)
        : xi{std::move(xi_)}, ui{std::move(ui_)} {
    }

    X xi;
    U ui;
};

template<typename X, typename U, typename D>
U idw(const std::vector<xu_pair<X, U>> &xus, const X &x, D d, float p = 1.0f) {
    U wiui_sum{};
    float wi_sum{};

    for (const xu_pair<X, U> &xu : xus) {
        auto d_ = d(x, xu.xi);
        if (d_ <= std::numeric_limits<float>::epsilon()) {
            return xu.ui;
        } else {
            auto wi_ = 1.0f / std::pow(d_, p * 0.5f);
            wiui_sum = wiui_sum + (xu.ui * wi_);
            wi_sum += wi_;
        }
    }

    return wiui_sum * (1.0f / wi_sum);
}


void plot_1d() {
    std::vector<xu_pair<float, float>> samples;
    samples.emplace_back(0.0f, 1.0f);
    samples.emplace_back(1.0f, 2.0f);
    samples.emplace_back(4.0f, 2.0f);
    samples.emplace_back(5.0f, 1.0f);

    Gnuplot gp;
    gp << "set term svg size 800, 400\n";
    gp << "set output 'idw_d1.svg'\n";
    gp << "set border 0\n";
    gp << "set grid ytics xtics\n";
    gp << "set xrange [-4:10]\n";
    gp << "set yrange [0.5:2.5]\n";
    gp << "set xlabel 'x'\n";
    gp << "set ylabel 'u'\n";
    gp << "set style line 1 lc rgb 'black' pt 7 ps .7\n";
    gp << "plot "
          "'-' with lines title 'p = 1.0', "
          "'-' with lines title 'p = 2.0', "
          "'-' with lines title 'p = 3.0', "
          "'-' with lines title 'p = 4.0', "
          "'-' with points ls 1 notitle \n";

    auto d_func = [](auto x, auto y) {
        auto dx = y - x;
        return dx * dx;
    };

    float x;
    const float dx = 0.01;

    // p = 1
    x = -4;
    while (x < 10) {
        gp << x << " " << idw(samples, x, d_func, 1.0) << '\n';
        x += dx;
    }
    gp << "e \n";

    // p = 2
    x = -4;
    while (x < 10) {
        gp << x << " " << idw(samples, x, d_func, 2.0) << '\n';
        x += dx;
    }
    gp << "e \n";

    // p = 3
    x = -4;
    while (x < 10) {
        gp << x << " " << idw(samples, x, d_func, 3.0) << '\n';
        x += dx;
    }
    gp << "e \n";

    // p = 4
    x = -4;
    while (x < 10) {
        gp << x << " " << idw(samples, x, d_func, 4.0) << '\n';
        x += dx;
    }
    gp << "e \n";

    // points
    for (auto &pt : samples) {
        gp << pt.xi << " " << pt.ui << '\n';
    }
}

struct Point2D {
    float x, y;
};

void plot_2d() {
    std::vector<xu_pair<Point2D, float>> samples;
    samples.emplace_back(Point2D{-2, 2}, -2.0f);
    samples.emplace_back(Point2D{3, -1}, -1.0f);
    samples.emplace_back(Point2D{-3, -3}, 5.0f);
    samples.emplace_back(Point2D{4, 2}, 5.0f);

    Gnuplot gp;
    gp << "set term svg size 800, 800 square\n";
    gp << "set output 'idw_d2.svg'\n";
    gp << "set border 0\n";
    gp << "set grid ytics xtics\n";
    gp << "set xrange [-6:6]\n";
    gp << "set yrange [-6:6]\n";
    gp << "set xlabel 'x'\n";
    gp << "set ylabel 'y'\n";

    gp << "set isosample 10\n";
    gp << "set samples 10\n";
    gp << "set cntrparam bspline levels 15\n";
    gp << "set cntrlabel onecolor\n";
    gp << "set palette gray\n";

//    gp << "unset surface\n";
    gp << "unset key\n";

    gp << "set view map\n";
    gp << "set contour base\n";

    gp << "set style line 1 lc rgb 'black' pt 7 ps .7\n";
    gp << "splot "
          "'-' with lines lc rgb 'black' lw 1 notitle nosurface, "
          "'-' with labels notitle, "
          "'-' with points ls 1 notitle nocontours, \n";

    auto d_func = [](auto& a, auto& b) {
      auto dx = b.x - a.x;
      auto dy = b.y - a.y;
      return dx * dx + dy * dy;
    };

    Point2D xy{};
    float delta = 0.1;

    const float mn_range = -6;
    const float mx_range = 6;
    const float p = 2.0;

    // contour
    xy = {mn_range, mn_range};
    while (xy.x < mx_range) {
        xy.y = mn_range;
        while (xy.y < mx_range) {
            auto u = idw(samples, xy, d_func, p);
            gp << xy.x << " " << xy.y << " " << u <<  '\n';
            xy.y += delta;
        }
        gp << '\n';

        xy.x += delta;
    }
    gp << "e \n";

    // labels
    delta = 1.0;
    xy = {mn_range, mn_range};
    while (xy.x < mx_range) {
        xy.y = mn_range;
        while (xy.y < mx_range) {
            auto u = idw(samples, xy, d_func, p);
            gp << xy.x << " " << xy.y << " " << u <<  '\n';
            xy.y += delta;
        }
        gp << '\n';

        xy.x += delta;
    }
    gp << "e \n";

    //     points
    for (auto &s : samples) {
        gp << s.xi.x << " " << s.xi.y << " " << s.ui << '\n';
        gp << "\n";
    }
    gp << "e \n";

}

int main() {
    plot_1d();
    plot_2d();

    return 0;
}
