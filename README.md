# Inverse Distance Weighting - IDW

[Inverse Distance Weighting](https://en.wikipedia.org/wiki/Inverse_distance_weighting)

## 1D

![](images/idw_d1.svg "")

## 2D

![](images/idw_d2.svg "")

